setlimit=10000000
maxcores=64
corelimit=(1 << 6) < maxcores ? (1 << 6) : maxcores
puts corelimit
puts setlimit

cores=1
set=64

`echo "" > test1.txt`
while cores <= corelimit
	puts "cores #{cores}"
	set=64
	while set <= setlimit
		puts "set #{set}" 	
		
		avgseq0 = 0
		avgseq1 = 0
		avgseq2 = 0
		avgseq3 = 0
		n = 0
		p = 0
		match0 = ""
		match1 = ""
		match2 = ""
		match3 = ""
		for i in 1..10  	
			output = `./cilk --nproc #{cores} #{set}`.split(";")
			p = output[0]
			n = output[1]
			avgseq0 += output[2].to_f
			match0 = output[3]
			avgseq1 += output[4].to_f
			match1 = output[5]
			avgseq2 += output[6].to_f
			match2 = output[7]
			avgseq3 += output[8].to_f
			match3 = output[9]
		end

		test = [ 
			p, 
			n, 
			"%6.10f" % (avgseq0/10.0), 
			match0,
			"%6.10f" % (avgseq1/10.0), 
			match1,
			"%6.10f" % (avgseq2/10.0), 
			match2,
			"%6.10f" % (avgseq3/10.0), 
			match3 
		 ].join(";").strip
		`echo "#{test}" >> test1.txt`

	      	set <<= 1	
	end
	cores <<= 1
end
