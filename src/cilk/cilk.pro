TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CC = cilkc
QMAKE_CFLAGS =

QMAKE_LIBDIR += /usr/lib/cilk
INCLUDEPATH += /usr/include/cilk
LIBS += -lcilk -lcilkrt0

SOURCES += \
	main.cilk \
    funcs.cilk

HEADERS += \
    base.h
