#ifndef BASEH_H
#define BASEH_H

// this is just to fool the syntax parser of my IDE
#ifndef __CILK__
#define cilk
#define spawn
#define sync
#define inlet
#endif

#include <cilk-lib.h>
#include <cilk.h>
#include <stdlib.h>
#include <stdio.h>

#endif // BASEH_H
