#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define MPITYPE MPI_INT
#define CTYPE int
#define PRINTTYPE "%d"

int main(int argc, char *argv[])
{

	long i, j, n, m;
	long nlocal;
	int* counts;
	CTYPE *blocal, *alocal, *xlocal;
	CTYPE *a, *b, *x;
	CTYPE *xref;
	double time_start, time_end;
	int p, rank;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	n = atol(argv[1]);
	m = atol(argv[2]);

	srand(time(0));

	nlocal = n/p;

	// allocate all the required
	a = (CTYPE*)malloc(n*m*sizeof(CTYPE));
	xref = (CTYPE*)malloc(n*sizeof(CTYPE));
	b = (CTYPE*)malloc(n*sizeof(CTYPE));
	x = (CTYPE*)malloc(n*sizeof(CTYPE));
	blocal = (CTYPE*)malloc(nlocal*sizeof(CTYPE));
	alocal = (CTYPE*)malloc(nlocal*n*sizeof(CTYPE));
	xlocal = (CTYPE*)malloc(n*sizeof(CTYPE));

	if(rank==0)
	{
		for(i=0;i<n;++i)
		{
			for(j=0;j<m;++j)
				a[j*n+i] = i*n+j; // random();
			b[i] = i;  // random();
		}
	}

	// now scatter the vector
	MPI_Scatter(b, nlocal, MPITYPE, blocal, nlocal, MPITYPE, 0, MPI_COMM_WORLD);
	MPI_Scatter(a, n*nlocal, MPITYPE, alocal, n*nlocal, MPITYPE, 0, MPI_COMM_WORLD);

	counts = malloc(p * sizeof(int));
	for (i = 0; i < p; i++) counts[i] = n;

	// wait for the scatter to complete to give all processes a fair start
	MPI_Barrier(MPI_COMM_WORLD);

	// kick of the measurement
	if(rank == 0)
		time_start = MPI_Wtime();

	for(i=0;i<n;i++)
	{
		xlocal[i] = 0.0;
		for(j=0;j<nlocal;++j)
			xlocal[i] += alocal[i+j*n]*blocal[j];
	}

	MPI_Reduce_scatter(xlocal, x, counts, MPITYPE, MPI_SUM, MPI_COMM_WORLD);

	// as we want to measure the allgather performance barrier here again
	MPI_Barrier(MPI_COMM_WORLD);

	// kick of the measurement
	if(rank == 0)
		time_end = MPI_Wtime();

	// output the result vector
	if(rank==0)
	{
		printf("result;%d;%ld;%8.8f;", p, n, time_end - time_start);

		time_start = MPI_Wtime();

		// calculate the verification result and compare it to gathered version
		for (i=0; i<n; i++) {
			xref[i] = 0.0;
			for (j=0; j<m; j++)
				xref[i] += a[j*n+i]*b[j];
		}

		time_end = MPI_Wtime();

		printf("%8.8f;", time_end - time_start);

		int match = 0;
		for(i=0;i<n;++i)
		{
			if(x[i] != xref[i])
				match++;
		}

		if(match)
			printf("mismatch\n");
		else
			printf("match\n");
	}

	MPI_Finalize();

	return 0;
}
