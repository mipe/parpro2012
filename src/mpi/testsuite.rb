setlimit=10000
maxcores=64
corelimit=(1 << 6) < maxcores ? (1 << 6) : maxcores
puts corelimit
puts setlimit

cores=1
set=64

`echo "" > test1.txt`
while cores <= corelimit
	puts "cores #{cores}"
	set=64
	while set <= setlimit
		puts "set #{set}" 	
		
		avgseq = 0
		avgmul = 0
		n = 0
		p = 0
		match = ""
		for i in 1..10  	
			output = `mpirun -np #{cores} ./matrix1 #{set}`.split(";")
			p = output[1]
			n = output[2]
			avgmul += output[3].to_f
			avgseq += output[4].to_f
			match = output[5]
		end

		test = [ "result", p, n, "%6.10f" % (avgmul/10.0), "%6.10f" % (avgseq/10), match ].join(";").strip
		`echo "#{test}" >> test1.txt`

	      	set <<= 1	
	end
	cores <<= 1
end
