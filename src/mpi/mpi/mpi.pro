TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CC=mpicc

SOURCES += \
	../matrix2.c

INCLUDEPATH+=/usr/include/mpi
LIBS += -lmpi

