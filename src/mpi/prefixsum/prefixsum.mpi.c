#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

// MPI header
#include <mpi.h>

#define HELLO 1234 // tag for control messages

void printArray(char* text, int rank, int* array, int n) {
	printf("\n%i %s: [ ",rank, text);
	for (int i = 0; i < n - 1; i++) {
		printf("%i", array[i]);
		printf(" | ");
	}
	printf("%i", array[n - 1]);
	printf(" ]\n");
}

void arrayscan(int A[], int n, int bsize, MPI_Comm comm) {
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);	
	
	int offset = rank*(n/size);
	int k = offset+1;
	
	int *B = malloc(sizeof(int)*bsize);
	
	int i=1;
	B[0] = A[k-1];
	for(k; k<offset+bsize; k++) {
		B[i] = A[k]+B[i-1];
	//	printf("\n%i %i + %i", k, A[k],B[i-1]);
  	i++;
	}
	
	int presum = 0;
	
//	printf("\n begin exscan");
//	MPI_Exscan(&B[bsize-1], &presum, 1, MPI_INT, MPI_SUM, comm);
// 	printf("\n%i end exscan %i\n",rank, presum);

 	for (int k = 1; k < size; k <<= 1) {
	  int tmpsum = B[bsize-1] + presum;
		if (rank < size - k) {
			MPI_Request req;
			MPI_Isend(&tmpsum, 1, MPI_INT, rank + k, k, comm, &req);
		}
		if (rank >= k) {
			MPI_Recv(&tmpsum, 1, MPI_INT, rank - k, k, comm, MPI_STATUS_IGNORE);
			presum = tmpsum + presum;
		}
	}
	
//	printArray(" B:",rank, B,bsize);
	
	//add sum of previous block and write back to A
	if(rank==0) {
		for(int i=0; i<bsize; i++) {
			A[i] = B[i];
		}
	} else {
		i = 0;
		for(k=offset; k<offset+bsize+1; k++) {
		  A[k] = B[i] + presum;
		  i++;
  	}
  }
}

int fRand(int fMin, int fMax) {
	double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(int argc, char *argv[])
{
  int rank, size;
  int prev;
  char name[MPI_MAX_PROCESSOR_NAME];
  int nlen;
  
  int n = 0;
  if(argc<2) {
  	printf("n needed");
  	return 1;
  } else {
	  n = atoi(argv[1]);
	}
  
  int *A = malloc(sizeof(int)*n);
  
  unsigned int iseed = (int) time(NULL);
	srand(iseed);
	
	//initdata
	for (int i = 0; i < n; i++) {
		A[i] = fRand(0,10);
	}

  MPI_Init(&argc,&argv);

  // get rank and size from communicator
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  double test_start_seq, test_end_seq;
  int *SRes = malloc(sizeof(int)*n);
  if(rank==0) {
		test_start_seq = MPI_Wtime();		
		int serial_sum = 0;
		for (int i = 0; i < n; i++) {
			serial_sum += A[i];
			SRes[i] = serial_sum;
		}
		test_end_seq = MPI_Wtime();
  }

  MPI_Get_processor_name(name,&nlen);

  if (rank==0) {
  //  printf("Rank %d initializing, total %d\n",rank,size);
    //printArray("input",rank,A,n);
  }
  
  int bsize = n/size;
	if(rank==size-1 && n%size!=0) {
		bsize = n/size + n%size;
	} else {
		bsize = n/size;
	}
//	printf("\n%i bsizes= %i", rank, bsize);
  
  //perftest
  double test_start = MPI_Wtime();  
  arrayscan(A,n,bsize,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);	
	
	int offset = (n/size)*rank;
	if (rank == 0) {
		int recvcounts[size], displs[size];
		for (int i = 0; i < size; i++) {
			recvcounts[i] = displs[i] = bsize * i;
		}
		
		MPI_Gatherv(MPI_IN_PLACE, bsize, MPI_INT, A, recvcounts, displs, MPI_INT, 0, MPI_COMM_WORLD);
		double test_end = MPI_Wtime();
				
		for(int i=0;i<n;i++) {
			if(A[i]==SRes[i]) {			
			} else {
				printf("MPI result does not match sequential result! aborting");
				exit(1);
			}
		}		
		//printArray("result",rank,A,n);
		printf("\n%f;%f;%i;%i",test_end-test_start,test_end_seq-test_start_seq,n,size);
  } else if (rank == size - 1) {
		MPI_Gatherv(&A[offset], bsize, MPI_INT, NULL, 0, 0, MPI_INT, 0, MPI_COMM_WORLD);
  } else {
		MPI_Gatherv(&A[offset], bsize, MPI_INT, NULL, 0, 0, MPI_INT, 0, MPI_COMM_WORLD);
	}
      
  MPI_Finalize();
  return 0;
}
