#!/bin/bash
N=( 12 13 14 15 16 1000 10000 100000 1000000 10000000 100000000)
CORES=( 1 2 4 8 16 32 64 )
#N=( 10000000 )
for (( i=0;i<${#N[@]};i++)); do
	for (( j=0;j<${#CORES[@]};j++)); do
		mpirun -n ${CORES[${j}]} prefixsum ${N[${i}]}
	done
done
