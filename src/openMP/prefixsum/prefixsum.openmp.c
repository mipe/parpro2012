#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <stdbool.h>
#include <time.h>
#include <limits.h>

#define PRIM_TYPE int
#define PRINTF_SYM "%i"

const int N[] = {1, 2, 4, 8, 16, 32, 48, 100, 999, 1000,10000000};

void resetArray(long *array, int length) {
	for(int i=0; i<length; i++) {
		array[i] = 0;
	}
}

void printArray(char* text, double* array, int n) {
	printf("\n%s: [ ", text);
	for (int i = 0; i < n - 1; i++) {
		printf("%f", array[i]);
		printf(" | ");
	}
	printf("%f", array[n - 1]);
	printf(" ]\n");
}

//void printArray(char* text, PRIM_TYPE* array, int n) {
//	printf("\n%s: [ ", text);
//	for (int i = 0; i < n - 1; i++) {
//		printf(PRINTF_SYM, array[i]);
//		printf(" | ");
//	}
//	printf(PRINTF_SYM, array[n - 1]);
//	printf(" ]\n");
//}

void printWork(long *work, int maxthreads) {
	printf("work: ");
	long sum = 0;
	for (int i = 0; i < maxthreads -1; i++) {
		sum+=work[i];
		//printf("%ld | ", work[i]);
	}
	printf("%ld\n", sum);
//	printf("%i\n", work[maxthreads - 1]);
}

void printStats(char* text, double start, double end) {
	printf("\n----- %s -----", text);
	printf("\nduration: %lf", end - start);
}

void printArrayStats(char* text, double start, double end, PRIM_TYPE* result, int n) {
	printStats(text, start, end);
//	printArray("result: ", result, n);
}

void printSingleStats(char* text, double start, double end, PRIM_TYPE result) {
	printStats(text, start, end);
	printf("\nsingleResult: ");
	printf(PRINTF_SYM,result);
	printf("\n");
}

void printStatStats(char *algo, double *times, int tLength, int cores, int n) {
	double min = times[0];
	double max = times[0];
	double sum = 0;
	for(int i=0;i<tLength;i++) {
		sum += times[i];
		if(times[i]>max) max=times[i];
		if(times[i]<min) min=times[i];
	}
	double avg = sum/tLength;
	printf("\n%s;%i;%i;%f;%f;%f",algo,cores,n,min,max,avg);	
}

bool odd(int x) {
	return x % 2 == 1;
}

void recursiveScan(PRIM_TYPE* x, int n, PRIM_TYPE* y, long *work) {
	if (n == 1)
		return;

	#pragma omp parallel for default (none) firstprivate (n,x,y,work)
	for (int i = 0; i < n / 2; i++) {
		y[i] = x[2 * i] + x[2 * i + 1];
		work[omp_get_thread_num()]++;
	}
	recursiveScan(y, n / 2, y + n / 2, work);
	x[1] = y[0];

	#pragma omp parallel for default (none) firstprivate (n,x,y,work)
	for (int i = 1; i < n / 2; i++) {
		x[2 * i] = y[i - 1] + x[2 * i];
		x[2 * i + 1] = y[i];
		work[omp_get_thread_num()]++;
	}
	if (odd(n))
		x[n - 1] = y[n / 2 - 1] + x[n - 1];		
}

void iterativeScan(PRIM_TYPE* x, int n, long *work) {
	int k, kk;
	//up-phase
	for (k = 1; k < n; k = kk) {
		kk = k << 1;
		#pragma omp parallel for default(none) firstprivate (n,k,kk,x,work)
		for (int i = kk - 1; i < n; i += kk) {
			x[i] = x[i - k] + x[i];
			work[omp_get_thread_num()]++;
		}
	}
	//down-pase
	for (k = k >> 1; k > 1; k = kk) {
		kk = k >> 1;
		#pragma omp parallel for default(none) firstprivate (n,k,kk,x,work)
		for (int i = k - 1; i < n - kk; i += k) {
			x[i + kk] = x[i] + x[i + kk];
			work[omp_get_thread_num()]++;
		}
	}
}

PRIM_TYPE* hillisSteeleScan(PRIM_TYPE *x, int n, long *work) {
	PRIM_TYPE *y = (PRIM_TYPE *) malloc(n * sizeof(PRIM_TYPE));
	PRIM_TYPE *t;
	for (int k = 1; k < n; k <<= 1) {
		#pragma omp parallel for default(none) firstprivate (n,k,y,x,work)
		for (int i = 0; i < k; i++) {
			y[i] = x[i];
			work[omp_get_thread_num()]++;
		}
		#pragma omp parallel for default(none) firstprivate (n,k,y,x,work)
		for (int i = k; i < n; i++) {
			y[i] = x[i - k] + x[i];
			work[omp_get_thread_num()]++;
		}
		t = x;
		x = y;
		y = t;
	}
	//printArray("\n", x);
	//printArray("\n", y);
	//printArray("\n", t);
	return x;
}

PRIM_TYPE fRand(PRIM_TYPE fMin, PRIM_TYPE fMax) {
	double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(int argc, char** argv) {
	double test_start, test_end;
	
	PRIM_TYPE iseed = (PRIM_TYPE) time(NULL);
	srand(iseed);
	
	int nrTests = 10;
	double * timeResults = malloc(sizeof(double) * nrTests);
	
	int nrThreads = omp_get_max_threads();
	
	for(int j=0;j<sizeof(N)/sizeof(int);j++) {
		int n = N[j];
		PRIM_TYPE *input = malloc(sizeof(PRIM_TYPE) * n), *z = malloc(sizeof(PRIM_TYPE) * n), *result = malloc(sizeof(PRIM_TYPE) * n);

		//initdata
		for (int i = 0; i < n; i++) {
			input[i] = fRand(0,10);
		}
		//		printf("\n------\n");
		//printf("\nnumber available processors: %i\n", omp_get_num_procs());
		//printf("number available threads: %i\n", omp_get_max_threads());
		//printf("n=%i",n);
			
		//printArray("input", input);
	
		//serial
		for(int k = 0; k<nrTests; k++) {
			test_start = omp_get_wtime();
			PRIM_TYPE serial_sum = 0;
			for (int i = 0; i < n; i++) {
				serial_sum += input[i];
				result[i] = serial_sum;
			}
			test_end = omp_get_wtime();
	
			//printArrayStats("serial computation", test_start, test_end, result);
			//printSingleStats("serial computation", test_start, test_end, result[n-1]);
			timeResults[k] = test_end - test_start;
		}
//		printArray("serial",timeResults, nrTests);
		printStatStats("serial",timeResults,nrTests,nrThreads,n);

		//parallel
		int maxThreads = omp_get_max_threads();
		long *work = malloc(sizeof(long) * maxThreads);
		resetArray(work,maxThreads);


		//reduction
		for(int k = 0; k<nrTests; k++) {
			test_start = omp_get_wtime();
			PRIM_TYPE red_sum = 0;

			#pragma omp parallel for reduction(+:red_sum) default (none) firstprivate (n,work,input)
			for (int i = 0; i < n; i++) {
				red_sum += input[i];
				work[omp_get_thread_num()]++;
			}
			test_end = omp_get_wtime();
			//printSingleStats("reduction computation", test_start, test_end, red_sum);
			//printWork(work,maxThreads);
			resetArray(work,maxThreads);
			timeResults[k] = test_end - test_start;
		}
//		printArray("reduction",timeResults, nrTests);
		printStatStats("reduction",timeResults,nrTests,nrThreads,n);
		
		//recursive
		PRIM_TYPE* y = (PRIM_TYPE *) malloc(n * sizeof(PRIM_TYPE));
		for(int k = 0; k<nrTests; k++) {
			for (int i = 0; i < n; i++) {
				z[i] = input[i];
			}
	
			test_start = omp_get_wtime();
			recursiveScan(z, n, y, work);
			test_end = omp_get_wtime();

			//printArrayStats("recursive computation",test_start, test_end, z);
			//printSingleStats("recursive computation", test_start, test_end, z[n - 1]);
			//printWork(work,maxThreads);
			resetArray(work,maxThreads);
			timeResults[k] = test_end - test_start;
		}
//		printArray("recursvie",timeResults, nrTests);
		printStatStats("recursive",timeResults,nrTests,nrThreads,n);

		//iterative
		for(int k = 0; k<nrTests; k++) {
			for (int i = 0; i < n; i++) {	
				y[i] = input[i];
			}
	
			test_start = omp_get_wtime();
			iterativeScan(y, n, work);
			test_end = omp_get_wtime();
			//printArrayStats("iterative computation", test_start, test_end, y);
			//printSingleStats("iterative computation", test_start, test_end, y[n - 1]);
			//printWork(work,maxThreads);
			resetArray(work,maxThreads);
			timeResults[k] = test_end - test_start;
		}
//		printArray("iterative",timeResults, nrTests);
		printStatStats("iterative",timeResults,nrTests,nrThreads,n);

		//hillis-steele
		for(int k = 0; k<nrTests; k++) {
			for (int i = 0; i < n; i++) {
				y[i] = input[i];
			}

			test_start = omp_get_wtime();
			result = hillisSteeleScan(y, n, work);
			test_end = omp_get_wtime();
			//printArrayStats("hillis-steele computation", test_start, test_end, result);
			//printSingleStats("hillis-steele computation", test_start, test_end,	result[n - 1]);
			//printWork(work,maxThreads);
			resetArray(work,maxThreads);
			timeResults[k] = test_end - test_start;
		}
		//printArray("hillis",timeResults, nrTests);
		printStatStats("hillis",timeResults,nrTests,nrThreads,n);
	}
	
	return 0;
}
