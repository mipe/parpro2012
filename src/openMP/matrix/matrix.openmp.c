#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <stdbool.h>
#include <time.h>

const int N[] = { 2, 4, 8, 16, 32, 48, 99, 100, 999, 1000, 10000}; //rows
const int M[] = { 2, 4, 8, 16, 32, 48, 100, 999, 1000, 10000}; //cols

void printStatStats(char *algo, double *times, int tLength, int cores, int n, int m) {
	//printf("\n%i", tLength);
	double min = times[0];
	double max = times[0];
	double sum = 0;
	for(int i=0;i<tLength;i++) {
		sum += times[i];
		if(times[i]>max) max=times[i];
		if(times[i]<min) min=times[i];
		//printf("\n%f",times[i]);
	}
	double avg = sum/tLength;
	printf("\n%s;%i;%i;%i;%f;%f;%f",algo,cores,n,m,min,max,avg);	
}

void printStats(char* text, double start, double end) {
	printf("\n----- %s -----", text);
	printf("\nduration: %lf", end - start);
}

void printVector(char* text, int* array, int m) {
	printf("\n%s:\n", text);
	for (int i = 0; i < m; i++) {
		printf("%i ", array[i]);
	}
}

void printMatrix(char* text, int** array, int m, int n) {
	printf("\n%s:\n", text);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++)
			printf("%i ", array[i][j]);
		printf("\n");
	}
}

int** initMatrix(bool rand, int m, int n) {
	int** array = malloc(n * sizeof(int *));
	for (int i = 0; i < n; i++) {
		array[i] = malloc(m * sizeof(int));
		for (int j = 0; j < m; j++) {
			if (rand)
				array[i][j] = random() % 10;
			else
				array[i][j] = 0;
		}
	}
	return array;
}

int main(int argc, char** argv) {
	unsigned int iseed = (unsigned int) time(NULL);
	srand(iseed);

	int nrTests=10;
	int **A;
	int **Result;
	double test_start, test_end;
	
	double * timeResults = malloc(sizeof(double) * nrTests);
	int nrThreads = omp_get_max_threads();


	for(int j=0;j<sizeof(M)/sizeof(int);j++) {
		int m = M[j];
		int *x = malloc(m * sizeof(int));
//		printf("\nm%i",m);
		
		for(int k=0;k<sizeof(N)/sizeof(int);k++) {
//			printf("\n%i",k);
			int n = N[k];	
//			printf("\nn%i",n);
					
			//initdata
			for (int i = 0; i < m; i++) {
				x[i] = random() % 10;
			}
		//	printVector("vector", x, m);
	
			A = initMatrix(true,m,n);

	//		printMatrix("matrix", A, m, n);

			Result = initMatrix(false,m,n);
			//printMatrix("empty",Result);

			//serial
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				for (int i = 0; i < n; i++) {
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}			
			printStatStats("serial",timeResults,nrTests,nrThreads,n,m);
//			printMatrix("result",Result,m,n);
			//printStats("serial",test_start,test_end);

			//outer implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				#pragma omp parallel for
				for (int i = 0; i < n; i++) {
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
				//printf("\n%f",timeResults[l]);
			}
			printStatStats("good_default",timeResults,nrTests,nrThreads,n,m);

			//inner implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				for (int i = 0; i < n; i++) {
					#pragma omp parallel for
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("bad_default",timeResults,nrTests,nrThreads,n,m);
			
			//outer implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				#pragma omp parallel for schedule (static)
				for (int i = 0; i < n; i++) {
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("good_static",timeResults,nrTests,nrThreads,n,m);

			//inner implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				for (int i = 0; i < n; i++) {
					#pragma omp parallel for schedule (static)
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("bad_static",timeResults,nrTests,nrThreads,n,m);
			
			//outer implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				#pragma omp parallel for schedule (dynamic)
				for (int i = 0; i < n; i++) {
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("good_dynamic",timeResults,nrTests,nrThreads,n,m);

			//inner implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				for (int i = 0; i < n; i++) {
					#pragma omp parallel for schedule (static)
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("bad_dynamic",timeResults,nrTests,nrThreads,n,m);
			
			//outer implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				#pragma omp parallel for schedule (guided)
				for (int i = 0; i < n; i++) {
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("good_guided",timeResults,nrTests,nrThreads,n,m);

			//inner implicit barrier
			for(int l = 0; l<nrTests; l++) {
				test_start = omp_get_wtime();
				for (int i = 0; i < n; i++) {
					#pragma omp parallel for schedule (guided)
					for(int j = 0; j < m; j++) {
						Result[i][j] = A[i][j]*x[j];
					}
				}
				test_end = omp_get_wtime();
				timeResults[l] = test_end - test_start;
			}
			printStatStats("bad_guided",timeResults,nrTests,nrThreads,n,m);
		}
	}
	return 0;
}
