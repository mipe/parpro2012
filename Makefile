CILK_C=cilkc
TARGET_DIR=$(CURDIR)/bin

all: cilk_test

# cilk targets
cilk: src/cilk/prefixsum.cilk.c src/cilk/matrixmul.cilk.c
	${CILK_C} src/cilk/prefixsum.cilk.c -o ${TARGET_DIR}/prefixsum_cilk
	${CILK_C} src/cilk/matrixmul.cilk.c -o ${TARGET_DIR}/matrixmul_cilk

cilk_clean:
	@echo "cleaning cilk ..."
	@rm ${TARGET_DIR}/prefixsum_cilk ${TARGET_DIR}/matrixmul_cilk

cilk_test: cilk
	@echo "running prefixsum_cilk ..."
	@${TARGET_DIR}/prefixsum_cilk
	@echo "running matrixmul_cilk ..."
	@${TARGET_DIR}/matrixmul_cilk

clean: cilk_clean
